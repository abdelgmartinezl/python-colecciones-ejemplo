

if __name__ == "__main__":
    petra = {'usuario': 'soy-petra', 'seguidores': 5000}
    calixtra = {'usuario': 'wololo', 'seguidores': 7500}
    print(petra)
    print(petra['usuario'])
    print(petra.keys())
    print(petra.values())
    print(petra.items())

    for valores in petra.values():
        print(valores)

    usuarios = [petra, calixtra]
    print(usuarios)
    for usuario in usuarios:
        print(usuario['usuario'])

