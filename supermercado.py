if __name__ == "__main__":
    print("Lista de Supermercado")
    sm = []

    archivo = open("super.txt", "r")
    for linea in archivo:
        sm.append(linea)
    archivo.close()
    print(sm)

    for n in range(3):
        articulo = input("Articulo: ")
        sm.append(articulo)

    print("\nMi Lista")
    num = 1
    for a in sm:
        print(str(num) + ". "+ a)
        num += 1

    while True:
        try:
            opc = int(input("\nArt. por Eliminar (n): "))
        except:
            print("Numero invalido")
            opc = len(sm)+1
        if opc <= len(sm) and opc > 0:
            break
        else:
            print("Articulo no existe")

    print("\nEstariamos eliminando " + sm[opc-1])
    sm.pop(opc-1)

    print("\nMi Lista")
    num = 1
    for a in sm:
        print(str(num) + ". "+ a)
        num += 1

    while True:
        try:
            opc = int(input("\nArt. por Sustituir (n): "))
        except:
            print("Numero invalido")
            opc = len(sm)+1
        if opc <= len(sm) and opc > 0:
            break
        else:
            print("Articulo no existe")

    print("\nEstariamos sustituyendo " + sm[opc-1])
    articulo = input("Nuevo Articulo: ")
    sm[opc-1] = articulo

    archivo = open("super.txt", "a+")
    for a in sm:
        archivo.write(a + "\n")
    archivo.close()

